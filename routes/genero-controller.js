const express = require("express");
const router = express.Router();

const model = require("../models/index");

router.all("/", (req, res, next) => {
    model.Genero.findAll()
        .then(lista => res.json({ ok: true, data: lista }))
        .catch(err => res.json({ ok: false, error: err }));
    res.send("Estas en /genero")
});
router.get("/:id", (req, res, next) => {
    let idGenero = req.params.id;
    model.Genero.findOne({ where: { id_genre: idGenero } })
        .then(item => res.json({ ok: true, data: item }))
        .catch(err => res.json({ ok: false, err: err }));
});
router.post("/", (req, res, next) => {
    model.Genero.create({ genre: req.body })
        .then(item => res.json({ ok: true, data: item }))
        .catch(err => res.json({ ok: false, err: err }));
});
router.put("/:id", (req, res, next) => {
    let idGenero = req.params.id;
    model.Genero.findOne({ where: { id_genre: idGenero } })
        .then(item => item.update(req.body))
        .then(item => res.json({ ok: true, data: item }))
        .catch(err => res.json({ ok: false, err: err }));
});
router.delete("/:id", (req, res, next) => {
    let idGenero = req.params.id;
    model.Genero.destroy({ where: { id_genre: idGenero } })
        .then(item => res.json({ ok: true, data: item }))
        .catch(err => res.json({ ok: false, err: err }));
});

module.exports = router;