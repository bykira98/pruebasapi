module.exports = (sequelize, DataTypes) => {
    const Genero = sequelize.define('Genero', {
        id_genre: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
        genre: DataTypes.STRING,
    }, { tableName: 'genres', timestamps: false });
    return Genero;
};